Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nautilus-hide
Source: https://github.com/brunonova/nautilus-hide

Files: *
Copyright: 2015-2017 Bruno Nova <brunomb.nova@gmail.com>
License: GPL-3.0+

Files: nautilus-hide.appdata.xml
Copyright: 2016 Bruno Nova <brunomb.nova@gmail.com>
License: GPL-3.0+

Files: debian/*
Copyright: 2015-2017 Bruno Nova <brunomb.nova@gmail.com>
           2017-2019 Carlos Maddela <e7appew@gmail.com>
License: GPL-3.0+

Files: extension/nautilus-hide.py
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
           2016 Steeven Lopes <steevenlopes@outlook.com>
License: GPL-3.0+

Files: po/nautilus-hide.pot
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
License: same-as-nautilus-hide

Files: po/de.po
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
Comment:
 Translators: Bruno Nova <brunomb.nova@gmail.com>, 2015.
              Silubr, 2017.
License: same-as-nautilus-hide

Files: po/es.po
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
Comment:
 Translators: Carlos Maddela <e7appew@gmail.com>, 2019.
License: same-as-nautilus-hide

Files: po/fr.po
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
Comment:
 Translators: Bruno Nova <brunomb.nova@gmail.com>, 2015.
              Okki <okki@gnomelibre.fr>, 2015.
License: same-as-nautilus-hide

Files: po/pt.po
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
Comment:
 Translators: Bruno Nova <brunomb.nova@gmail.com>, 2015.
License: same-as-nautilus-hide

Files: po/sk.po
Copyright: 2015 Bruno Nova <brunomb.nova@gmail.com>
Comment:
 Translators: Dušan Kazik <prescott66@gmail.com>, 2017.
License: same-as-nautilus-hide

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: same-as-nautilus-hide
 This file is distributed under the same license as the nautilus-hide package.
